package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    private TextView texto;
    private boolean bandera = false;
    private String operacion = "";
    private int estado = 1;
    private String operacionTotal;
    private double resultadoOperacion = Double.NaN;
    private double memoria = Double.NaN;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto = findViewById(R.id.textCalculadora);


        if(savedInstanceState != null){
            resultadoOperacion = savedInstanceState.getDouble("resultadoOperacion");
            bandera = savedInstanceState.getBoolean("bandera");
            estado = savedInstanceState.getInt("estado");
            operacion = savedInstanceState.getString("operacion");
            texto.setText(savedInstanceState.getString("texto"));
            operacionTotal = savedInstanceState.getString("operacionTotal");
            memoria = savedInstanceState.getDouble("memoria");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putDouble("resultadoOperacion", resultadoOperacion);
        outState.putBoolean("bandera", bandera);
        outState.putInt("estado", estado);
        outState.putString("operacion" , operacion);
        outState.putString("operacionTotal" , operacionTotal);
        outState.putString("texto" , texto.getText().toString());
        outState.putDouble("memoria", memoria);
    }


    public void onClickResultado(View view) {



        if (operacion.equals("√")) { // si la operacion es la raiz

            String textoPantalla = texto.getText().toString();
            String primerDigito = textoPantalla.substring(0, textoPantalla.indexOf(operacion));

            if (!primerDigito.equals("")) {

                double primerNumero = Double.parseDouble(primerDigito);
                String segundoDigito = (textoPantalla.substring(textoPantalla.indexOf(operacion) + 1));
                double segundoNumero = Double.parseDouble(segundoDigito);
                resultadoOperacion = primerNumero * (Math.sqrt(segundoNumero));
                formatoResultado(resultadoOperacion, textoPantalla);


            } else {
                String segundoDigito = (textoPantalla.substring(textoPantalla.indexOf(operacion) + 1));
                double segundoNumero = Double.parseDouble(segundoDigito);
                resultadoOperacion = Math.sqrt(segundoNumero);
                formatoResultado(resultadoOperacion, textoPantalla);
            }

        } else if (estado == 3) {

            String textoPantalla = texto.getText().toString();
            String segundoDigito = (textoPantalla.substring(textoPantalla.indexOf(operacion) + 1));
            String primerDigito = textoPantalla.substring(0, textoPantalla.indexOf(operacion));
            double primerNumero = Double.parseDouble(primerDigito);
            double segundoNumero = Double.parseDouble(segundoDigito);

            switch (operacion) {
                case "+":
                    resultadoOperacion = primerNumero + segundoNumero;
                    formatoResultado(resultadoOperacion, textoPantalla);
                    break;
                case "-":
                    resultadoOperacion = primerNumero - segundoNumero;
                    formatoResultado(resultadoOperacion, textoPantalla);
                    break;
                case "x":
                    resultadoOperacion = primerNumero * segundoNumero;
                    formatoResultado(resultadoOperacion, textoPantalla);
                    break;
                case "/":
                    if (segundoNumero == 0) {
                        String resultado = "NaN";
                        Toast.makeText(getApplicationContext(), "No se puede dividir por 0: ", Toast.LENGTH_LONG).show();
                        texto.setTextSize(30);
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(3);
                        texto.setFilters(fArray);
                        texto.setText(resultado);
                        resultadoOperacion = 0;
                        bandera = false;
                        estado = 1;

                    } else {
                        resultadoOperacion = primerNumero / segundoNumero;
                        formatoResultado(resultadoOperacion, textoPantalla);
                    }

                    break;
                case "^":
                    resultadoOperacion = Math.pow(primerNumero, segundoNumero);
                    formatoResultado(resultadoOperacion, textoPantalla);
                    break;


                default:
                    resultadoOperacion = 0;

            }


        }


    }


    public void onClickRaizOperacion(View view) {
        String textoPantalla = texto.getText().toString();
        resultadoOperacion = Double.NaN;
        if (estado == 2 && bandera) {
            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);
            textoPantalla += "√";
            operacion = "√";
            texto.setText(textoPantalla);
        } else {
            if (!textoPantalla.contains("√")) {
                bandera = true;
                operacion = "√";
                textoPantalla += operacion;
                texto.setText(textoPantalla);
            }
        }
    }

    public void onClickMemoria(View view){
        String textoPantalla = texto.getText().toString();
        if(estado == 2 && !Double.isNaN(resultadoOperacion)){
            memoria = Double.parseDouble(textoPantalla);
        }else {
            if(!Double.isNaN(memoria) && estado != 3){
                if(memoria % 1 == 0){
                    texto.setText(String.valueOf((int)memoria));
                }else {
                    texto.setText(String.valueOf(memoria));
                }
                resultadoOperacion = memoria;
                siguienteOperacion();
            }
        }
    }

    public void onClickOperacion(View view) {

        if (estado == 2) {
            int boton = view.getId();
            String textoPantalla;
            switch (boton) {

                case R.id.botonSuma:

                    textoPantalla = texto.getText().toString();

                    if (bandera) {
                        String caracter = textoPantalla.substring(textoPantalla.length() - 1);
                        if (!caracter.equals("+")) {
                            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);

                            textoPantalla += "+";
                            bandera = true;
                            operacion = "+";
                            texto.setText(textoPantalla);

                        }

                    } else {

                        operacion = "+";
                        bandera = true;
                        textoPantalla += operacion;
                        texto.setText(textoPantalla);
                    }


                    break;



                case R.id.botonResta:

                    textoPantalla = texto.getText().toString();
                    if (bandera) {
                        String caracter = textoPantalla.substring(textoPantalla.length() - 1);
                        if (!caracter.equals("-")) {
                            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);
                            textoPantalla += "-";
                            operacion = "-";
                            bandera = true;
                            texto.setText(textoPantalla);
                        }
                    } else {
                        operacion = "-";
                        bandera = true;
                        textoPantalla += operacion;
                        texto.setText(textoPantalla);
                    }
                    break;

                case R.id.botonMulti:

                    textoPantalla = texto.getText().toString();
                    if (bandera) {
                        String caracter = textoPantalla.substring(textoPantalla.length() - 1);
                        if (!caracter.equals("x")) {
                            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);
                            textoPantalla += "x";
                            operacion = "x";
                            bandera = true;
                            texto.setText(textoPantalla);
                        }
                    } else {

                        bandera = true;
                        operacion = "x";
                        textoPantalla += operacion;
                        texto.setText(textoPantalla);

                    }
                    break;


                case R.id.botonDivision:
                    textoPantalla = texto.getText().toString();
                    if (bandera) {
                        String caracter = textoPantalla.substring(textoPantalla.length() - 1);
                        if (!caracter.equals("/")) {
                            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);
                            textoPantalla += "/";
                            operacion = "/";
                            bandera = true;
                            texto.setText(textoPantalla);
                        }
                    } else {

                        bandera = true;
                        operacion = "/";
                        textoPantalla += operacion;
                        texto.setText(textoPantalla);
                    }
                    break;


                case R.id.botonPotencia:

                    textoPantalla = texto.getText().toString();
                    if (bandera) {
                        String caracter = textoPantalla.substring(textoPantalla.length() - 1);
                        if (!caracter.equals("^")) {
                            textoPantalla = textoPantalla.substring(0, textoPantalla.length() - 1);
                            textoPantalla += "^";
                            operacion = "^";
                            bandera = true;
                            texto.setText(textoPantalla);
                        }
                    } else {

                        bandera = true;
                        operacion = "^";
                        textoPantalla += operacion;
                        texto.setText(textoPantalla);
                    }

                    break;

                default:


            }
            resultadoOperacion = Double.NaN;

        }


    }

    public void onClickBoton(View view) {


        if(!Double.isNaN(resultadoOperacion)){
            onClickLimpiar(view);
        }

            int boton = view.getId();
            String resultado;
            switch (boton) {
                case R.id.boton0:
                    resultado = texto.getText().toString() + "0";
                    texto.setText(resultado);
                    break;
                case R.id.boton1:
                    resultado = texto.getText().toString() + "1";
                    texto.setText(resultado);
                    break;
                case R.id.boton2:
                    resultado = texto.getText().toString() + "2";
                    texto.setText(resultado);
                    break;
                case R.id.boton3:
                    resultado = texto.getText().toString() + "3";
                    texto.setText(resultado);
                    break;
                case R.id.boton4:
                    resultado = texto.getText().toString() + "4";
                    texto.setText(resultado);
                    break;
                case R.id.boton5:
                    resultado = texto.getText().toString() + "5";
                    texto.setText(resultado);
                    break;
                case R.id.boton6:
                    resultado = texto.getText().toString() + "6";
                    texto.setText(resultado);
                    break;
                case R.id.boton7:
                    resultado = texto.getText().toString() + "7";
                    texto.setText(resultado);
                    break;
                case R.id.boton8:
                    resultado = texto.getText().toString() + "8";
                    texto.setText(resultado);
                    break;
                case R.id.boton9:
                    resultado = texto.getText().toString() + "9";
                    texto.setText(resultado);
                    break;

                default:

            }

            estado = 2;


            if (!operacion.equals("")) {
                estado = 3;
            }

    }


    public void onClickLimpiar(View view) {
// restablece todas las variables a sus valores iniciales
        texto.setText("");
        bandera = false;
        estado = 1;
        resultadoOperacion = Double.NaN;
        operacion = "";
        texto.setTextSize(50);
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(12);
        texto.setFilters(fArray);
    }

    public void siguienteOperacion() {

        bandera = false;
        operacion = "";
        estado = 2;
    }

    public void onClickMostrar(View view) {

            if(operacionTotal == null){
                operacionTotal = "Ninguna operación realizada!";
            }

            Intent intent = new Intent(this, SegundoActivity.class);
            intent.putExtra("resultado", operacionTotal);
            startActivity(intent);

    }

    public void formatoResultado (double resultadoOperacion, String textoPantalla){
        String resultado;
        if(resultadoOperacion % 1 == 0) {
            resultado = String.valueOf((int)resultadoOperacion);
        }else{
            resultado = String.valueOf(resultadoOperacion);
        }

        texto.setText(resultado);
        operacionTotal = textoPantalla + " = " + resultado;
        siguienteOperacion();
    }
}
