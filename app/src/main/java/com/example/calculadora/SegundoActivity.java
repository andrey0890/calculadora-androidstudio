package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SegundoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);

        Intent intent = getIntent();
        String resultado = intent.getStringExtra("resultado");

        TextView textoResultado = findViewById(R.id.textOperacion);
        textoResultado.setText(resultado);
    }

    public void onClickRegresar(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
